<?php

$f3=require('lib/base.php');

$f3->set('DEBUG',3);
$f3->set('UI','ui/');

$f3->set('DB',
  new DB\SQL(
    'mysql:host=localhost;port=3306;dbname=YourDatabaseName',
    'YourUserName',
    'YourPassword'
  )
);

//home page
$f3->route('GET /',
  function ($f3) {
    $f3->set('html_title','Home Page');
    $article=new DB\SQL\Mapper($f3->get('DB'),'article');
    $articles=$article->find();
    $f3->set('articles',$articles);
    $f3->set('content','blog_home.html');
    echo Template::instance()->render('layout.html');
  }
);

$f3->route('GET /view/@id',
  function ($f3) {
    $id = $f3->get('PARAMS.id');
    //create Mapper object and search for id
    $article=new DB\SQL\Mapper($f3->get('DB'),'article');
    $article->load(array('id=?', $id));
    //set framework variables
    $f3->set('html_title',$article->title);
    $article->copyTo('POST');
    //display the view
    $f3->set('content','blog_detail.html');
    echo Template::instance()->render('layout.html');
  }
);

// Admin Home
$f3->route('GET /admin',
  function ($f3) {
    $f3->set('html_title','My Blog Administration');
    //assign a mapper to the user table
    $user=new DB\SQL\Mapper($f3->get('DB'),'user');
    //tell Auth what database fields to use
    $auth=new \Auth($user,
      array('id'=>'name','pw'=>'password'));
    //will display an HTTP 401 Unauthorized error page if unsuccessful
    if ($auth->basic()) {
      //store the name they logged in with
      $f3->set('SESSION.user','SERVER.PHP_AUTH_USER');
    } else {
      $f3->error(401);
    }

    $article=new DB\SQL\Mapper($f3->get('DB'),'article');
    $list=$article->find();
    $f3->set('list',$list);
    //display the admin view
    $f3->set('content','admin_home.html');
    echo template::instance()->render('layout.html');
  }
);

//Admin Add
$f3->route('GET /admin/add',
  function($f3) {
    if (!$f3->get('SESSION.user')) $f3->error(401);
    $f3->set('html_title','My Blog Create');
    $f3->set('content','admin_edit.html');
    echo template::instance()->render('layout.html');
  }
);

//Admin Edit 
$f3->route('GET /admin/edit/@id',
  function($f3) {
    if (!$f3->get('SESSION.user')) $f3->error(401);
    $f3->set('html_title','My Blog Edit');
    $id = $f3->get('PARAMS.id');
    $article=new DB\SQL\Mapper($f3->get('DB'),'article');
    $article->load(array('id=?',$id));
    $article->copyTo('POST');
    $f3->set('content','admin_edit.html');
    echo template::instance()->render('layout.html');
  }
);

//Admin Add and Edit both deal with Form Posts
//don't use a lambda function here because there are 2 routes
$f3->route('POST /admin/edit/@id','edit');
$f3->route('POST /admin/add','edit');
function edit($f3) {
  //check they are allowed access
  if (!$f3->get('SESSION.user')) $f3->error(401);
  $id = $f3->get('PARAMS.id');
  //create an article object
  $article=new DB\SQL\Mapper($f3->get('DB'),'article');
  //if we don't load it first Mapper will do an insert instead of update when we use save command
  if ($id) $article->load(array('id=?',$id));
  //overwrite with values just submitted
  $article->copyFrom('POST');
  //create a timestamp in MySQL format
  $article->timestamp=date("Y-m-d H:i:s");
  $article->save();
  // Return to admin home page, new blog entry should now be there
  $f3->reroute('/admin');
}

//Admin Delete
$f3->route('GET /admin/delete/@id',
  function($f3) {
    if (!$f3->get('SESSION.user')) $f3->error(401);
    $id = $f3->get('PARAMS.id');
    $article=new DB\SQL\Mapper($f3->get('DB'),'article');
    $article->load(array('id=?',$id));
    $article->erase();
    $f3->reroute('/admin');
  }
);

$f3->run();
